﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HappyCat.Migrations
{
    public partial class addednewfk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CatGallery_Cats_CatsId",
                table: "CatGallery");

            migrationBuilder.DropIndex(
                name: "IX_CatGallery_CatsId",
                table: "CatGallery");

            migrationBuilder.DropColumn(
                name: "CatsId",
                table: "CatGallery");

            migrationBuilder.AddColumn<int>(
                name: "CatId",
                table: "CatGallery",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CatGallery_CatId",
                table: "CatGallery",
                column: "CatId");

            migrationBuilder.AddForeignKey(
                name: "FK_CatGallery_Cats_CatId",
                table: "CatGallery",
                column: "CatId",
                principalTable: "Cats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CatGallery_Cats_CatId",
                table: "CatGallery");

            migrationBuilder.DropIndex(
                name: "IX_CatGallery_CatId",
                table: "CatGallery");

            migrationBuilder.DropColumn(
                name: "CatId",
                table: "CatGallery");

            migrationBuilder.AddColumn<int>(
                name: "CatsId",
                table: "CatGallery",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CatGallery_CatsId",
                table: "CatGallery",
                column: "CatsId");

            migrationBuilder.AddForeignKey(
                name: "FK_CatGallery_Cats_CatsId",
                table: "CatGallery",
                column: "CatsId",
                principalTable: "Cats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
