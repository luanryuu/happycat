﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Models;

namespace Test.Repository
{
    public interface ICatRepository
    {
        Task<int> AddNewCatAsync(CatModel model);
        Task<List<CatModel>> DeleteCat(CatModel catModel);
        Task<List<CatModel>> GetAllCats();
        Task<CatModel> GetCatById(int id);
        Task<List<CatModel>> GetTopCatsAsync(int count);

        string GetAppName();
    }
}