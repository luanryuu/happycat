﻿using HappyCat.Data;
using HappyCat.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Models;

namespace Test.Repository
{
    public class CatRepository : ICatRepository
    {
        private readonly HappyCatContext _context = null;

        public CatRepository(HappyCatContext context)
        {
            _context = context;
        }

        public async Task<int> AddNewCatAsync(CatModel model)
        {
            var newCat = new Cats()
            {
                Id = model.Id,
                Name = model.Name,
                Type = model.Type,
                Info = model.Info,
                CoverImageUrl = model.CoverImageUrl
            };
            newCat.catGallery = new List<CatGallery>();
            if (model.Gallery != null)
            {
                foreach (var file in model.Gallery)
                {
                    newCat.catGallery.Add(new CatGallery()
                    {
                        Name = file.Name,
                        URL = file.URL
                    });
                }
            }

            await _context.Cats.AddAsync(newCat);
            await _context.SaveChangesAsync();

            return model.Id;
        }

        public async Task<List<CatModel>> GetAllCats()
        {
            return await _context.Cats.Select(cat => new CatModel()
            {
                Id = cat.Id,
                Name = cat.Name,
                Type = cat.Type,
                Info = cat.Info,
                CoverImageUrl = cat.CoverImageUrl
            }).ToListAsync();
        }

        public async Task<List<CatModel>> GetTopCatsAsync(int count)
        {
            return await _context.Cats.Select(cat => new CatModel()
            {
                Id = cat.Id,
                Name = cat.Name,
                Type = cat.Type,
                Info = cat.Info,
                CoverImageUrl = cat.CoverImageUrl
            }).Take(count).ToListAsync();
        }

        public async Task<CatModel> GetCatById(int id)
        {
            return await _context.Cats.Where(x => x.Id == id)
                .Select(cat => new CatModel()
                {
                    Id = cat.Id,
                    Name = cat.Name,
                    Type = cat.Type,
                    Info = cat.Info,
                    CoverImageUrl = cat.CoverImageUrl,
                    Gallery = cat.catGallery.Select(g => new GalleryModel()
                    {
                        Id = g.Id,
                        Name = g.Name,
                        URL = g.URL
                    }).ToList()
                }).FirstOrDefaultAsync();
        }

        public async Task<List<CatModel>> DeleteCat(CatModel catModel)
        {

            Cats model = _context.Cats.First(x => x.Id == catModel.Id);

            _context.Cats.Remove(model);
            await _context.SaveChangesAsync();

            return await _context.Cats.Select(cat => new CatModel()
            {
                Id = cat.Id,
                Name = cat.Name,
                Type = cat.Type,
                Info = cat.Info,
                CoverImageUrl = cat.CoverImageUrl
            }).ToListAsync();

        }

        public string GetAppName()
        {
            return "Happy Cat application";
        }
    }
}