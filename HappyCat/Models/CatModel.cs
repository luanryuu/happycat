﻿using HappyCat.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class CatModel
    {
        [Key]
        public int Id { get; set; }
        [StringLength(30, MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter name. (2-30 characters)")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please choose type")]
        public string Type { get; set; }
        [Display(Name="Information")]
        public string Info { get; set; }
        [Display(Name = "Cover photo")]
        public IFormFile CoverPhoto { get; set; }
        public string CoverImageUrl { get; set; }
        [Display(Name = "Gallery")]
        public List<IFormFile> GalleryFiles { get; set; }
        public List<GalleryModel> Gallery { get; set; }
    }
}
