﻿namespace HappyCat.Services
{
    public interface IUserService
    {
        string GetUserId();
        bool IsAuthenticated();
    }
}