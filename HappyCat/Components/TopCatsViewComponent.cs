﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Test.Repository;

namespace HappyCat.Components
{
    public class TopCatsViewComponent : ViewComponent
    {
        private readonly ICatRepository _catRepository;
        public TopCatsViewComponent(ICatRepository catRepository)
        {
            _catRepository = catRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(int count)
        {
            var cats = await _catRepository.GetTopCatsAsync(count);
            return View(cats);
        }
    }
}
