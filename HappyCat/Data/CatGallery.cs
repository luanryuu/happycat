﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyCat.Data
{
    public class CatGallery
    {
        public int Id { get; set; }
        public int CatId { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public Cats Cat { get; set; }
    }
}
