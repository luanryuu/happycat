﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyCat.Data
{
    public class Cats
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Info { get; set; }
        public string CoverImageUrl { get; set; }
        public ICollection<CatGallery> catGallery { get; set; }
    }
}
