﻿using HappyCat.Data;
using HappyCat.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class HappyCatContext : IdentityDbContext<ApplicationUser>
    {
        public HappyCatContext(DbContextOptions<HappyCatContext> options): base(options)
        {

        }

        public DbSet<Cats> Cats { get; set; }
        public DbSet<CatGallery> CatGallery { get; set; }
    }
}
