﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using HappyCat.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Test.Models;
using Test.Repository;

namespace HappyCat.Controllers
{
    public class CatController : Controller
    {
        private readonly ICatRepository _catRepository = null;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public IActionResult Index()
        {
            return View();          
        }

        public CatController(ICatRepository catRepository, IWebHostEnvironment webHostEnvironment)
        {
            _catRepository = catRepository;
            _webHostEnvironment = webHostEnvironment;
        }
        [Authorize]
        public ViewResult AddNewCat(bool isSuccess = false)
        {
            ViewBag.IsSuccess = isSuccess;
            var path = Path.Combine(Directory.GetCurrentDirectory(), $"wwwroot\\{"/lib/json/cat_breeds.json"}");
            var json = System.IO.File.ReadAllText(path);
            dynamic breedsJson = JsonConvert.DeserializeObject(json);
            ViewBag.Breeds = breedsJson;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddNewCat(CatModel catModel)
        {
            if (ModelState.IsValid)
            {
                if (catModel.CoverPhoto != null)
                {
                    string folder = "cats/cover/";
                    catModel.CoverImageUrl = await UploadImage(folder, catModel.CoverPhoto);
                }
                if (catModel.GalleryFiles != null && catModel.GalleryFiles.Count > 0)
                {
                    string gallery_folder = "cats/gallery/";
                    catModel.Gallery = new List<GalleryModel>();
                    foreach (IFormFile file in catModel.GalleryFiles)
                    {
                        var gallery = new GalleryModel()
                        {
                            Name = file.FileName,
                            URL = await UploadImage(gallery_folder, file)
                        };
                        catModel.Gallery.Add(gallery);
                    }
                }           
                int id = await _catRepository.AddNewCatAsync(catModel);
                if (id > 0)
                {
                    return RedirectToAction("AddNewCat", new { isSuccess = true });
                }
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), $"wwwroot\\{"/lib/json/cat_breeds.json"}");
            var json = System.IO.File.ReadAllText(path);
            dynamic breedsJson = JsonConvert.DeserializeObject(json);
            ViewBag.Breeds = breedsJson;
            return View();
        }

        private async Task<string> UploadImage(string folderPath, IFormFile file)
        {          
            folderPath += Guid.NewGuid().ToString() + "_" + file.FileName;
            string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, folderPath);
            await file.CopyToAsync(new FileStream(serverFolder, FileMode.Create));
            return "/" + folderPath;
        }

        [Route("all-cats")]
        public async Task<ViewResult> GetAllCats()
        {
            var data = await _catRepository.GetAllCats();
            return View(data);
        }

        [Route("cat-details/{id:int:min(1)}", Name = "catDetailsRoute")]
        public async Task<ViewResult> GetCat(int id)
        {
            var data = await _catRepository.GetCatById(id);
            return View(data);
        }

        [Route("cat-delete/{id:int:min(1)}", Name = "catDeleteRoute")]
        public async Task<ViewResult> DeleteCat(CatModel catModel)
        {
            var data= await _catRepository.DeleteCat(catModel);
            return View("GetAllCats",data);
        }
    }
}
