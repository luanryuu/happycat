﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HappyCat.Models;
using HappyCat.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test.Models;

namespace Test.Controllers
{
    public class HomeController : Controller
    {
        readonly ILogger<HomeController> _logger;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;

        public HomeController(ILogger<HomeController> logger, IUserService userService,IEmailService emailService)
        {
            _logger = logger;
            _userService = userService;
            _emailService = emailService;
        }

        public IActionResult Index()
        {
            //var userId = _userService.GetUserId();
            //var isLoggedIn = _userService.IsAuthenticated();
            //UserEmailOptions options = new UserEmailOptions
            //{
            //    ToEmails = new List<string>() { "test@gmail.com" },
            //    PlaceHolders = new List<KeyValuePair<string, string>>()
            //    {
            //        new KeyValuePair<string, string>("{{UserName}}","Luan")
            //    }
            //};

            //await _emailService.SendTestEmail(options);
            return View();
        }    
        public ViewResult AboutUs()
        {
            return View();
        }


        public ViewResult ContactUs()
        {
            return View();
        }
    }
}
